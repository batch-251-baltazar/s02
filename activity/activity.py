#  1. Accept a year input from the user and determine if it is a leap year or not.

year = int (input("Please input a year:"))

if (year % 4) == 0:
	print(f"{year} is a leap year")
else:
	print(f"{year} is not a leap year.")

# 2. Accept two numbers (row and col) from the user and create a grid of asterisks using the two numbers (row and col).

row = int(input("Enter number of rows:\n"))
col = int(input("Enter number of columns:\n"))

for x in range(row):
    for y in range(col):
        print('*',end = ' ')
    print()